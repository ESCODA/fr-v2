---
layout: article
title: Afficheur LCD 16x2 Grove en C/C++
description: Exercice avec l'afficheur LCD 16x2 Grove en C/C++ avec Stm32duino
permalink: lcd_16x2.html
key: page-aside
cover: fr/assets/stickers/soon.jpg
aside:
  toc: true
sidebar:
  nav: site_nav
---
<div align="center">
<img alt="Grove - LCD 16x2 Display" src="images/grove-lcd_16x2_display.jpg" width="600px">
</div>

Bientôt disponible

> Crédit image : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
