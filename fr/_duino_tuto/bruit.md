---
layout: article
title: capteur sonore C/C++
descripttion: Exercice avec le capteur sonore en C/C++ pour Stm32duino
permalink: capteur_sonore.html
key: page-aside
cover: fr/assets/stickers/capteur_sonore.png
aside:
  toc: true
sidebar:
  nav: site_nav
---

- **Prérequis :**

Cf tuto du tilt-sensor. Cependant il faut brancher le capteur sur le pin A0.


- **Capteur de son :**

![Image](fr/assets/imagescapteur_sonore.png)


Ce capteur peut être utilisé comme détecteur de niveau sonore, en effet sa sortie est proportionnelle au niveau sonore environnant.

*Voici le code sur Arduino*
```c
void setup() {
  Serial.begin(9600);
  pinMode(A0,INPUT);                //détecteur de son en entrée por lire sa valeur

}

void loop() {
  Serial.println(analogRead(A0));   //on affiche sa valeur (entre 0 et 1024).
  delay(100);                       //En effet le résutlat n'est pas en décibel.

}
```

On peut observer l'évolution des valeurs renvoyées par le capteur de son via le traceur série accessible dans Outils/Traceur série ou grace au raccourcis Ctrl+Maj+N.

![Image](fr/assets/imagesgraphe_capteur_sonore.png)

> Crédit image : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
