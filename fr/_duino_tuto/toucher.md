---
layout: article
title: Capteur tactile Grove en C/C++
description: Exercice avec le capteur tactile Grove en C/C++ pour Stm32duino
permalink: capteur_tactile.html
key: page-aside
cover: fr/assets/stickers/capteur_tactile.png
aside:
  toc: true
sidebar:
  nav: site_nav
---
- **Prérequis :**

**Capteur tactile (Touch sensor):**
Ce capteur ne possède que 3 broches qu'il faut connecter avec des cables sur la carte shield de façon à ce que GND et VCC (rouge et noir) correspondent bien. Le cable jaune (branché à la broche SIG) sera connecté à D4.

![Image](fr/assets/images/stm32duino/capteur_tactile.png)

**Vibreur (vibration motor):**
Ce vibreur est exactement comme ceux que l'on peut retrouver dans nos téléphones. Il sera brancher sur D3.
![Image](fr/assets/images/stm32duino/vibration_motor.png)


Le capteur de toucher sera configuré en entrée et le vibreur en sortie: si il y a contact avec le capteur de tactile le vibreur s'activera.

*Voici le code sur Arduino*
```c
void setup() {
  Serial.begin(9600);
  pinMode(D4,INPUT);
  pinMode(D3,OUTPUT);

}

void loop() {

  if(digitalRead(D4))         
  {
    digitalWrite(D3,HIGH);
  }
  else
  {
    digitalWrite(D3,LOW);
  }
  delay(500);
}
```

> Crédit image : [Seedstudio](http://wiki.seeedstudio.com/Grove_System/)
