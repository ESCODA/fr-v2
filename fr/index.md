---
layout: article
title: STM32python - HOME
permalink: /
key: home
mode: immersive
article_header:
  actions:
    - text: Getting Started
      type: success
      url: "#themes"
  type: overlay
  theme: dark
  background_color: '#203028'
  background_image:
  #  gradient: 'linear-gradient(135deg, rgba(34, 139, 87 , .4), rgba(139, 34, 139, .4))'
    src: /fr/assets/cover/home.jpg

---


# Bienvenue sur le site STM32python

<p align="center">
<img src="fr/assets/logos/stm32upython.svg" width="400px" alt="logo"/>
</p>


## Contexte

La réforme des lycées introduit un nouvel enseignement suivi par tous les élèves de seconde générale et technologique :
[SNT (Sciences Numériques et Technologie)](https://eduscol.education.fr/cid143713/snt-bac-2021.html).
Un des thèmes abordés par cet enseignement est l’[Internet des objets (IdO, en anglais Internet of things, IoT)](https://fr.wikipedia.org/wiki/Internet_des_objets),
couvert par le chapitre "Informatique embarquée et objets connectés",
qui représente l’extension d’Internet à des choses et à des lieux du monde physique.

L’objectif est d’amener ces jeunes à un premier niveau de compréhension de l’internet des objets.
L’enjeu est de favoriser une orientation choisie, en l’occurrence ici vers l’ingénierie du numérique.
La part du « numérique » et de « l’informatique » dans les enseignements a été fortement augmentée avec la réforme du lycée.

## Objectif

L'objectif de STM32python est de fournir aux enseignants du lycée et aux lycéens des supports pédagogiques open-source pour l'initiation
à l’Internet des Objets pour l’enseignement de
[SNT (Sciences Numériques et Technologie)](https://eduscol.education.fr/cid143713/snt-bac-2021.html).
Ces supports s'appuient sur la plateforme NUCLEO WB55 (STM32WB55) de STMicroelectronics.
Ils permettent de réaliser des montages électroniques et des programmes pour les microcontroleurs STM32 avec les langages C/C++ et microPython.

Les supports réalisés sont également utilisables par d’autres enseignements de première et terminale générales, notamment en spécialité NSI (Numérique et Sciences Informatiques), en spécialité SI (Sciences de l’ingénieur), ou en série technologique STI2D (Sciences et Technologies de l’Industrie et du Développement Durable).


<div class="layout--articles">
  <section class="my-5">
    <header><h2 id = "themes">Les Thèmes :</h2></header>
    {%- include article-list.html articles=site.pages type='grid' -%}
  </section>
</div>

<div class="layout--articles">
  <section class="my-5">
    <header><h2 id="article-layout">Autres liens</h2></header>
    {%- include article-list.html articles=site.about type='grid' -%}
  </section>
</div>

## Partenaires
Les partenaires de STM32python sont:
* les rectorats des académies de [Grenoble](http://www.ac-grenoble.fr) et d’[Aix-Marseille](http://www.ac-aix-marseille.fr),
* [STMicroelectronics](https://www.st.com),
* [Inventhys](http://www.inventhys.com),
* [Polytech Grenoble](https://www.polytech-grenoble.fr), [Grenoble INP Institut d'ingénierie et de management](https://www.grenoble-inp.fr/), [Université Grenoble Alpes](https://www.univ-grenoble-alpes.fr).
