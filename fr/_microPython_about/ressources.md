---
layout : article
title: Ressources
description: Ressources
permalink: ressources.html
cover: fr/assets/stickers/Link.jpg
key: page-aside
aside:
  toc: true
sidebar:
  nav: site_nav
---

## Documentation

**Site officiel de MicroPython :**

  - [https://micropython.org/](https://micropython.org/)

**Documentation générale sur MicroPython :**

  - [http://docs.micropython.org/en/latest/](http://docs.micropython.org/en/latest/)

**Exemple d’utilisation de la librairie pyb (très utile) :**

_Attention le pinout de la NUCLEO-WB55 est différent de la cartepyB V1.1 :_

  - [http://docs.micropython.org/en/latest/pyboard/quickref.html#general-board-control](http://docs.micropython.org/en/latest/pyboard/quickref.html#general-board-control)

**Code source du projet MicroPython :**

  - [https://github.com/micropython/micropython](https://github.com/micropython/micropython)

**Site officiel de Python 3 :**

  - [https://www.python.org/](https://www.python.org/)

**Documentation technique de la NUCLEO-WB55 :**

  - [https://www.st.com/en/evaluation-tools/p-nucleo-wb55.html](https://www.st.com/en/evaluation-tools/p-nucleo-wb55.html)

**Documentation de l’application Android STBLESensor :**

  - [https://github.com/STMicroelectronics/STBlueMS_Android](https://github.com/STMicroelectronics/STBlueMS_Android)


## Mappage et désignation des broches pour les connecteurs Arduino et Morpho


Voici la correspondance des connecteurs avec le numéro des Pins du microcontrôleur:

![Tableau](fr/assets/images/microPython/pins.png)


![Tableau](fr/assets/images/microPython/tableau1.png)

![Tableau](fr/assets/images/microPython/tableau2.png)
